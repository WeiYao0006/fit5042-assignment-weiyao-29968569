package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "customerController")
@RequestScoped
public class CustomerController {

	private int customerIndex;
	private fit5042.assignment.repository.entities.Customer customer;
	
	private ArrayList<String> industries;

	public CustomerController() {
        // Assign property identifier via GET param 
        //this propertyID is the index, don't confuse with the Property Id
		customerIndex = Integer.valueOf(FacesContext.getCurrentInstance()
                .getExternalContext()
                .getRequestParameterMap()
                .get("customerIndex"));
        // Assign customer based on the index provided 
		customer = getCustomer();
		industries = getIndustries();
    }
	
	public ArrayList<String> getIndustries(){
		return new ArrayList<String>() {
			{
				add("Bank");
				add("Building");
				add("Data Communication");
				add("Education");
				add("Farm");
				add("Health");
				add("Mining");
				add("Publishing");
			}
		};
	}
	
	public void setIndustries(ArrayList<String> industries){
		this.industries = industries;
	}

	/**
	 * @return the customerIndex
	 */
	public int getCustomerIndex() {
		return customerIndex;
	}

	/**
	 * @param customerIndex the customerIndex to set
	 */
	public void setCustomerIndex(int customerIndex) {
		this.customerIndex = customerIndex;
	}

	/**
	 * @return the customer
	 */
	public fit5042.assignment.repository.entities.Customer getCustomer() {
		 if (customer == null) {
	            // Get application context bean CustomerApplication 
	            ELContext context
	                    = FacesContext.getCurrentInstance().getELContext();
	            AUSApplication app
	                    = (AUSApplication) FacesContext.getCurrentInstance()
	                            .getApplication()
	                            .getELResolver()
	                            .getValue(context, null, "ausApplication");
	            // -1 to customerIndex since we +1 in JSF (to always have positive property id!) 
	            return app.getCustomers().get(--customerIndex); //this customerIndex is the index, don't confuse with the Customer Id
	        }
	        return customer;
	    }

	

}
