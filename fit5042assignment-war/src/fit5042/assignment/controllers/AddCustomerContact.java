package fit5042.assignment.controllers;

import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.el.ELContext;
import javax.enterprise.context.ConversationScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.Customer;

@ConversationScoped
@Named("addCustomerContact")
public class AddCustomerContact implements Serializable {
	
	@ManagedProperty(value = "#{customerContactManagedBean}")
	CustomerContactManagedBean customerContactManagedBean;
	
	private AUSApplication app;
	private int customerid;
	private Customer customer;
	private fit5042.assignment.repository.entities.CustomerContact customerContact;

	public AddCustomerContact() {
		// instantiate customerManagedBean
		ELContext elContext = FacesContext.getCurrentInstance().getELContext();
		customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "customerContactManagedBean");
		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver()
				.getValue(elContext, null, "ausApplication");
		customerContact = new fit5042.assignment.repository.entities.CustomerContact();
		customerid = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerID"));
		setCustomer(app.getCustomerByID(customerid));
	}
	
	public void addContactPerson() {
		try {
			customerContact.setCustomer(getCustomer());
			customerContactManagedBean.addCustomerContact(customerContact);

			app.searchAll();
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Contact has been added succesfully"));
		} catch (Exception ex) {
			Logger.getLogger(AddCustomerContact.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	/**
	 * @return the customerid
	 */
	public int getCustomerid() {
		return customerid;
	}

	/**
	 * @param customerid the customerid to set
	 */
	public void setCustomerid(int customerid) {
		this.customerid = customerid;
	}

	/**
	 * @return the customer
	 */
	public Customer getCustomer() {
		return customer;
	}

	/**
	 * @param customer the customer to set
	 */
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public fit5042.assignment.repository.entities.CustomerContact getCustomerContact() {
		return customerContact;
	}

	public void setCustomerContact(fit5042.assignment.repository.entities.CustomerContact customerContact) {
		this.customerContact = customerContact;
	}
	
	

	
}
