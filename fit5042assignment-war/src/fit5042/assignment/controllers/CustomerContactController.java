package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.repository.entities.CustomerContact;

@Named(value = "customerContactController")
@RequestScoped
public class CustomerContactController {

	private int customerContactIndex;
	private fit5042.assignment.repository.entities.CustomerContact customerContact;
	
	public CustomerContactController() {
		customerContactIndex = Integer.valueOf(
				FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("customerContactId"));
		customerContact = getCustomerContact();
	}

	public int getCustomerContactIndex() {
		return customerContactIndex;
	}

	public void setCustomerContactIndex(int customerContactIndex) {
		this.customerContactIndex = customerContactIndex;
	}

	public fit5042.assignment.repository.entities.CustomerContact getCustomerContact() {
		if (customerContact == null) {
			// Get application context bean PropertyApplication
			ELContext context = FacesContext.getCurrentInstance().getELContext();
			AUSApplication app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver()
					.getValue(context, null, "ausApplication");
			// -1 to customerIndex since we +1 in JSF (to always have positive property id!)
			return app.getCustomerContactByID(customerContactIndex);
			// return app.getCustomers().get(--customerIndex); //this customerIndex is the
			// index, don't confuse with the Customer Id
		}
		return customerContact;
	}
	
	
}
