package fit5042.assignment.controllers;

import java.util.ArrayList;

import javax.el.ELContext;
import javax.enterprise.context.ApplicationScoped;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;



@Named(value = "ausApplication")
@ApplicationScoped
public class AUSApplication {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    @ManagedProperty(value = "#{customerContactManagedBean}")
    CustomerContactManagedBean customerContactManagedBean;
    
    private ArrayList<Customer> customers;
    private ArrayList<CustomerContact> customerContacts;
    
//    private boolean showForm = true;
//
//    public boolean isShowForm() {
//        return showForm;
//    }
    
    // Add some customer data from db to app 
    public AUSApplication() throws Exception {
    	customers = new ArrayList<>();
    	customerContacts = new ArrayList<>();
        //instantiate customerManagedBean
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");

        customerContactManagedBean = (CustomerContactManagedBean) FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerContactManagedBean");
        //get properties from db 
        updateCustomerList();
        updateCustomerContactList();
    }
    
    public void updateCustomerContactList() {
    	if (customerContacts != null && customerContacts.size() > 0) {
    		
    	}
    	else {
    		customerContacts.clear();
    		
    		for (CustomerContact contact: customerContactManagedBean.getAllCustomerContacts()) {
    			customerContacts.add(contact);
    		}
    		setCustomerContacts(customerContacts);
    	}
    }

	public void updateCustomerList() {
    	if (customers != null && customers.size() > 0)
    	{
    		
    	}
    	else 
    	{
            customers.clear();

            for (Customer customer : customerManagedBean.getAllCustomers())
            {
                customers.add(customer);
            }

            setCustomers(customers);
    	}
    }
    
    public void searchCustomerById(int customerId) {
    	customers.clear();
    	
    	customers.add(customerManagedBean.searchCustomerById(customerId));
    	setCustomers(customers);
    }
    
    public void searchCustomerByIndustry(String industry) {
    	customers.clear();
    	
    	for(fit5042.assignment.repository.entities.Customer customer: customerManagedBean.searchCustomerByIndustry(industry)) {
    		customers.add(customer);
    	}
    	setCustomers(customers);
    }
    
	/**
	 * @return the customers
	 */
	public ArrayList<Customer> getCustomers() {
		updateCustomerList();
		// do more, access managedbean
		return customers;
	}

	/**
	 * @param customers the customers to set
	 */
	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}
    
    public void searchAll()
    {
    	customers.clear();
        
        for (Customer customer : customerManagedBean.getAllCustomers())
        {
        	customers.add(customer);
        }
        
        setCustomers(customers);
    }

	public CustomerContact getCustomerContactByID(int customerContactIndex) {
		CustomerContact contact = new CustomerContact();
		for (CustomerContact con:getCustomerContacts()) {
			if (con.getCustomerContactId() == customerContactIndex) {
				return con;
			}
		}
		return contact;
	}

	public ArrayList<CustomerContact> getCustomerContacts() {
		return customerContacts;
	}

	public void setCustomerContacts(ArrayList<CustomerContact> customerContacts) {
		this.customerContacts = customerContacts;
	}

	public void searchAll2() {
		customerContacts.clear();
        
        for (fit5042.assignment.repository.entities.CustomerContact customerContact : customerContactManagedBean.getAllCustomerContacts())
        {
        	customerContacts.add(customerContact);
        }
        setCustomerContacts(customerContacts);
		
	}
	
	public fit5042.assignment.repository.entities.Customer getCustomerByID(int customerID) {
		fit5042.assignment.repository.entities.Customer customer = new Customer();
    	for(fit5042.assignment.repository.entities.Customer cus:getCustomers()) {
    		if (cus.getCustomerId() == customerID) {
    			return cus;
    		}
    	}
		return customer;
    }
}
