package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerManagedBean;

@RequestScoped
@Named("removeCustomer")
public class RemoveCustomer {
    @ManagedProperty(value = "#{customerManagedBean}")
    CustomerManagedBean customerManagedBean;
    
    AUSApplication app;
    
    public RemoveCustomer() {
        ELContext context
        = FacesContext.getCurrentInstance().getELContext();
    	
        app = (AUSApplication)FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "ausApplication");
        
        app.updateCustomerList();
        
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerManagedBean = (CustomerManagedBean)FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerManagedBean");
    }
    
    public void removeCustomer(int customerId) {
    	try {
    		customerManagedBean.removeCustomer(customerId);
    		
    		app.searchAll();
    		
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer has been deleted succesfully"));
    	} catch (Exception ex) {

        }
    }
}
