package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import fit5042.assignment.mbeans.CustomerContactManagedBean;
import fit5042.assignment.mbeans.CustomerManagedBean;


@RequestScoped
@Named("removeCustomerContact")
public class RemoveCustomerContact {
    @ManagedProperty(value = "#{customerContactManagedBean}")
    CustomerContactManagedBean customerContactManagedBean;
    
    AUSApplication app;

	public RemoveCustomerContact() {
        ELContext context
        = FacesContext.getCurrentInstance().getELContext();
    	
        app = (AUSApplication)FacesContext.getCurrentInstance()
                .getApplication()
                .getELResolver()
                .getValue(context, null, "ausApplication");
        
        app.updateCustomerContactList();
        
        ELContext elContext = FacesContext.getCurrentInstance().getELContext();
        customerContactManagedBean = (CustomerContactManagedBean)FacesContext.getCurrentInstance().getApplication()
                .getELResolver().getValue(elContext, null, "customerContactManagedBean");
		
	}
    
    public void removeCustomerContact(int customerContactId) {
    	try {
    		customerContactManagedBean.removeProperty(customerContactId);
    		
    		app.searchAll2();
    		
    		FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Customer Contact has been deleted succesfully"));
    	} catch (Exception ex) {

        }
    }

}
