package fit5042.assignment.controllers;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import fit5042.assignment.repository.entities.CustomerContact;

/**
 * @author Administrator
 *
 */
@RequestScoped
@Named(value = "customer")
public class Customer  implements Serializable{
	
	private int customerId;
	private String Name;
	private String industry;
	private String roomNo;
	private String street;
	private String suburb;
	private String postcode;
	private String state;
	
	private ArrayList<CustomerContact> customerContact;
	
	private ArrayList<String> industries;

    private Set<fit5042.assignment.repository.entities.Customer> customers;
	
	public Customer() {
		
	}
	
	public Customer(int customerId, String Name, String roomNo, String industry, String street, String suburb, String postcode, String state) {
		this.customerId = customerId;
		this.Name = Name;
		this.roomNo = roomNo;
		this.industry = industry;
		this.street = street;
		this.suburb = suburb;
		this.postcode = postcode;
		this.state = state;
		customerContact = new ArrayList<CustomerContact>();
		industries = getIndustries();
	}
	
	public ArrayList<String> getIndustries(){
		return new ArrayList<String>() {
			{
				add("Bank");
				add("Building");
				add("Data Communication");
				add("Education");
				add("Farm");
				add("Health");
				add("Mining");
				add("Publishing");
			}
		};
	}
	
	public void setIndustries(ArrayList<String> industries){
		this.industries = industries;
	}
	
	public Set<fit5042.assignment.repository.entities.Customer> getCustomer(){
		return customers;
	}
	
	public void setCustomer(Set<fit5042.assignment.repository.entities.Customer> customers) {
		this.customers = customers;
	}
	
	public String getName() {
		return Name;
	}
	
	public void setName(String Name) {
		this.Name = Name;
	}
	
	public String getRoomNo() {
		return roomNo;
	}
	
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	
	public String getIndustry() {
		return industry;
	}
	
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public String getPostcode() {
		return postcode;
	}
	
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	public ArrayList<CustomerContact> getCustomerContact(){
		return customerContact;
	}
	
	public void setCustomerContact(ArrayList<CustomerContact> customerContact) {
		this.customerContact = customerContact;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerId + ", Name=" + Name + ", industry=" + industry + ", roomNo=" + roomNo + ", street=" + street + ", suburb=" + suburb + ", postcode=" + postcode + ", state=" + state + "]";
	}
	
}
