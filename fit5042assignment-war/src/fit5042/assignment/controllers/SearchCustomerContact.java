package fit5042.assignment.controllers;

import javax.el.ELContext;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@RequestScoped
@Named("searchCustomerContact")
public class SearchCustomerContact {

	private fit5042.assignment.repository.entities.CustomerContact customerContact;
	AUSApplication app;
	private int searchByInt;
	
	public SearchCustomerContact() {
		ELContext context = FacesContext.getCurrentInstance().getELContext();

		app = (AUSApplication) FacesContext.getCurrentInstance().getApplication().getELResolver().getValue(context,
				null, "ausApplication");

		app.updateCustomerContactList();
		
	}
	
	public int getSearchByInt() {
		return searchByInt;
	}
	public void setSearchByInt(int searchByInt) {
		this.searchByInt = searchByInt;
	}
	public fit5042.assignment.repository.entities.CustomerContact getCustomerContact() {
		return customerContact;
	}
	public void setCustomerContact(fit5042.assignment.repository.entities.CustomerContact customerContact) {
		this.customerContact = customerContact;
	}

	public AUSApplication getApp() {
		return app;
	}

	public void setApp(AUSApplication app) {
		this.app = app;
	}
	
	public void searchAll() {
		try {
			app.searchAll2();
		} catch (Exception ex) {
			
		}
	}
	
}
