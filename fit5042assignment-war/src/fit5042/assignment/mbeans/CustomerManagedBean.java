package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.CustomerRepository;
import fit5042.assignment.repository.entities.Customer;
@ManagedBean(name = "customerManagedBean")
@SessionScoped
public class CustomerManagedBean implements Serializable {

	@EJB
	CustomerRepository customerRepository;

	public CustomerManagedBean() {
	}

	public List<Customer> getAllCustomers() {
		try {
			List<Customer> customers = customerRepository.getAllCustomers();
			return customers;
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public void removeCustomer(int customerId) {
		try {
			customerRepository.removeCustomer(customerId);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public Customer searchCustomerById(int id) {
		try {
			return customerRepository.searchCustomerById(id);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return null;
	}

	public void editCustomer(Customer customer) {
		try {
			customerRepository.editCustomer(customer);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Customer has been updated succesfully"));

		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void addCustomer(Customer customer) {
		try {
			customerRepository.addCustomer(customer);
			;
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private Customer converCustomerToEntity(fit5042.assignment.controllers.Customer localCustomer) {
		Customer customer = new Customer();
		customer.setName(localCustomer.getName());
		customer.setRoomNo(localCustomer.getRoomNo());
		customer.setIndustry(localCustomer.getIndustry());
		customer.setStreet(localCustomer.getStreet());
		customer.setSuburb(localCustomer.getSuburb());
		customer.setPostcode(localCustomer.getPostcode());
		customer.setState(localCustomer.getState());
		return customer;
	}

	public void addCustomer(fit5042.assignment.controllers.Customer localCustomer) {
		Customer customer = converCustomerToEntity(localCustomer);
		try {
			customerRepository.addCustomer(customer);
		} catch (Exception ex) {
			Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public List<Customer> searchCustomerByIndustry(String industry){
		try {
			return customerRepository.searchCustomerByIndustry(industry);
		} catch (Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
			
		}
		return null;
	}
}
