package fit5042.assignment.mbeans;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import fit5042.assignment.repository.CustomerContactRepository;
import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;

@ManagedBean(name = "customerContactManagedBean")
@SessionScoped
public class CustomerContactManagedBean implements Serializable{

    @EJB
    CustomerContactRepository customerContactRepository;
    
    public CustomerContactManagedBean() {
    	
    }
    
    public List<CustomerContact> getAllCustomerContacts(){
    	try {
    		List<CustomerContact> customerContacts = customerContactRepository.getAllCustomerContacts();
    		return customerContacts;
    	}catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    	return null;
    }
    
    public CustomerContact searchCustomerContactById(int id) {
    	try {
    		return customerContactRepository.searchCustomerContactById(id);
    	} catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    	return null;
    }
    
    public void removeProperty(int id) {
    	try {
    		customerContactRepository.removeCustomerContact(id);
    	} catch (Exception ex) {
            Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
    	}
    }

	public void editCustomerContact(CustomerContact customerContact) {
		try {
			customerContactRepository.editCustomerContact(customerContact);

			FacesContext.getCurrentInstance().addMessage(null,
					new FacesMessage("Customer Contact has been updated succesfully"));

		} catch (Exception ex) {
			Logger.getLogger(CustomerContactManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	public void addCustomerContact(CustomerContact customerContact) {
		try {
			customerContactRepository.addCustomerContact(customerContact);
		} catch(Exception ex) {
            Logger.getLogger(CustomerManagedBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		
	}
}
