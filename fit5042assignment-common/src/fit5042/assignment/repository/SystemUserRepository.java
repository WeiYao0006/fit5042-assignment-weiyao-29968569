package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.SystemUser;

@Remote
public interface SystemUserRepository {
	
    public List<SystemUser> getAllUser() throws Exception;
    
    public void removeUser(int userId) throws Exception;
    
    public void editUser(SystemUser systemUser) throws Exception;
    
    public void addUser(SystemUser systemUser) throws Exception;
    
    public SystemUser searchSystemUserById(int userId) throws Exception;

}
