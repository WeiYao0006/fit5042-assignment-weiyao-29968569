package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Remote;

import fit5042.assignment.repository.entities.Customer;

@Remote
public interface CustomerRepository {
	
    public List<Customer> getAllCustomers() throws Exception;
    
    public Customer searchCustomerById(int id) throws Exception;
    
    public List<Customer> searchCustomerByIndustry(String industry) throws Exception;
    
    public void removeCustomer(int customerId) throws Exception;
    
    public void editCustomer(Customer customer) throws Exception;
    
    public void addCustomer(Customer customer) throws Exception;

}
