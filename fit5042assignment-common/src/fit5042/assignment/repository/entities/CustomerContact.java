package fit5042.assignment.repository.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

/**
 * @author Wei Yao
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = CustomerContact.GET_ALL_CONTACT_PERSON, query = "SELECT c FROM CustomerContact c order by c.customerContactId desc ") })
public class CustomerContact implements Serializable{

	public static final String GET_ALL_CONTACT_PERSON =  "CustomerContact.getAll";
	
	private int customerContactId;
	private String firstName;
	private String lastName;
	private String gender;
	private String email;
	private String phoneNo;
	private Customer customer;
	
	public CustomerContact() {
		
	}
	
	public CustomerContact(int customerContactId, String firstName, String lastName, String gender, String email, String phoneNo, Customer customer) {
		this.customerContactId = customerContactId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.gender = gender;
		this.email = email;
		this.phoneNo = phoneNo;
		this.customer = customer;
	}
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customerContact_Id")
	public int getCustomerContactId() {
		return customerContactId;
	}
	
	public void setCustomerContactId(int customerContactId) {
		this.customerContactId = customerContactId;
	}

	@NotEmpty(message = "First Name should not be empty")
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@NotEmpty(message = "Last Name should not be empty")
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@NotEmpty(message = "Gender should not be empty")
	public String getGender() {
		return gender;
	}
	
	public void setGender(String gender) {
		this.gender = gender;
	}

	@NotEmpty(message = "Email should not be empty")
	@Email(message = "This is not a valid Email address")
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}

	@NotEmpty(message = "Phone number should not be empty")
	public String getPhoneNo() {
		return phoneNo;
	}
	
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	@ManyToOne(cascade = CascadeType.ALL)
	public Customer getCustomer() {
		return customer;
	}
	
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if(getClass() != obj.getClass()) {
			return false;
		}
		final CustomerContact other = (CustomerContact) obj;
		if (this.customerContactId != other.customerContactId) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Customer [customerContactId=" + customerContactId + ", firstName=" + firstName + ", lastName=" + lastName  + ", gender=" + gender + ", email=" + email + ", phoneNo=" + phoneNo + ", Customer=" + customer.getCustomerId() +"]";
	}
	
}