package fit5042.assignment.repository.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

@Entity
@NamedQueries({
	@NamedQuery(name = SystemUser.GET_ALL_QUERY_NAME, query = "SELECT s FROM SystemUser s order by s.userId ") })
public class SystemUser {

	public static final String GET_ALL_QUERY_NAME =  "SystemUser.getAll";
	
	private int userId;
	private String userPassword;
	private String role;
	private String phoneNo;
	private String email;
	private String TNF;
	
	public SystemUser() {
		
	}
	
	public SystemUser(int userId, String userPassword, String role, String phoneNo, String email, String TNF) {
		this.userId = userId;
		this.userPassword = userPassword;
		this.role = role;
		this.phoneNo = phoneNo;
		this.email = email;
		this.TNF = TNF;
	}
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "user_id")
	public int getUserId() {
		return userId;
	}
	
	public void setUserId(int userId) {
		this.userId = userId;
	}
	
	public String getUserPassword() {
		return userPassword;
	}
	
	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}
	
	public String getPhoneNo() {
		return phoneNo;
	}
	
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	public String getTNF() {
		return TNF;
	}
	
	public void setTNF(String TNF) {
		this.TNF = TNF;
	}
}
