/**
 * 
 */
package fit5042.assignment.repository.entities;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Wei Yao
 *
 */
@Entity
@NamedQueries({
	@NamedQuery(name = Customer.GET_ALL_QUERY_NAME, query = "SELECT c FROM Customer c order by c.customerId") })
public class Customer  implements Serializable{

	public static final String GET_ALL_QUERY_NAME =  "Customer.getAll";
	
	private int customerId;
	private String Name;
	private String industry;
	private String roomNo;
	private String street;
	private String suburb;
	private String postcode;
	private String state;
	
	private ArrayList<CustomerContact> customerContact;
	
	public Customer() {
		
	}
	
	public Customer(int customerId, String Name, String roomNo, String industry, String street, String suburb, String postcode, String state) {
		this.customerId = customerId;
		this.Name = Name;
		this.roomNo = roomNo;
		this.industry = industry;
		this.street = street;
		this.suburb = suburb;
		this.postcode = postcode;
		this.state = state;
		customerContact = new ArrayList<CustomerContact>();
	}
	

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@Column(name = "customer_id")
	public int getCustomerId() {
		return customerId;
	}
	
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	
	public String getName() {
		return Name;
	}
	
	public void setName(String Name) {
		this.Name = Name;
	}
	
	public String getRoomNo() {
		return roomNo;
	}
	
	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}
	
	public String getIndustry() {
		return industry;
	}
	
	public void setIndustry(String industry) {
		this.industry = industry;
	}
	
	public String getStreet() {
		return street;
	}
	
	public void setStreet(String street) {
		this.street = street;
	}
	
	public String getSuburb() {
		return suburb;
	}
	
	public void setSuburb(String suburb) {
		this.suburb = suburb;
	}
	
	public String getPostcode() {
		return postcode;
	}
	
	public void setPostcode(String postcode) {
		this.postcode = postcode;
	}
	
	public String getState() {
		return state;
	}
	
	public void setState(String state) {
		this.state = state;
	}
	
	@OneToMany(mappedBy = "customer",cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	public ArrayList<CustomerContact> getCustomerContact(){
		return customerContact;
	}
	
	public void setCustomerContact(ArrayList<CustomerContact> customerContact) {
		this.customerContact = customerContact;
	}

	@Override
	public String toString() {
		return "Customer [customerID=" + customerId + ", Name=" + Name + ", industry=" + industry + ", roomNo=" + roomNo + ", street=" + street + ", suburb=" + suburb + ", postcode=" + postcode + ", state=" + state + "]";
	}
	
}
