package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.Customer;
import fit5042.assignment.repository.entities.CustomerContact;

@Stateless
public class CustomerContactRepositorySessionBean implements CustomerContactRepository{

	@PersistenceContext (unitName = "fit5042assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;

	@Override
	public List<CustomerContact> getAllCustomerContacts() throws Exception {
		return entityManager.createNamedQuery(CustomerContact.GET_ALL_CONTACT_PERSON)
				.getResultList();	
	}

	@Override
	public CustomerContact searchCustomerContactById(int id) throws Exception {
		CustomerContact customerContact =  entityManager.find(CustomerContact.class, id);
		return customerContact;
	}

	@Override
	public void removeCustomerContact(int customerContactId) throws Exception {
		CustomerContact customerContact = this.searchCustomerContactById(customerContactId);
		if (customerContact != null) {
			entityManager.remove(customerContact);
		}
	}

	@Override
	public void editCustomerContact(CustomerContact customerContact) throws Exception {
		try {
			entityManager.merge(customerContact);
		} catch(Exception ex) {
			
		}
		
	}

	@Override
	public void addCustomerContact(CustomerContact customerContact) throws Exception {
		List<CustomerContact> contactPersons = entityManager.createNamedQuery(CustomerContact.GET_ALL_CONTACT_PERSON)
				.getResultList();
		customerContact.setCustomerContactId(contactPersons.get(0).getCustomerContactId() + 1);
		Customer customer = customerContact.getCustomer();
		customer.getCustomerContact().add(customerContact);
		entityManager.merge(customer);
	}

}
