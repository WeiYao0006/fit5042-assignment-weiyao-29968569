package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import fit5042.assignment.repository.entities.Customer;

@Stateless
public class CustomerRepositorySessionBean implements CustomerRepository {

	@PersistenceContext (unitName = "fit5042assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;

	@Override
	public List<Customer> getAllCustomers() throws Exception {
		return entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList();	
	}

	@Override
	public Customer searchCustomerById(int id) throws Exception {
		Customer customer = entityManager.find(Customer.class, id);
		return customer;
	}

	@Override
	public void removeCustomer(int customerId) throws Exception {
		Customer customer = this.searchCustomerById(customerId);
		if (customer != null) {
			entityManager.remove(customer);
		}
	}

	@Override
	public void editCustomer(Customer customer) throws Exception {
		try {
			entityManager.merge(customer);
		} catch(Exception ex) {
			
		}
		
	}

	@Override
	public void addCustomer(Customer customer) throws Exception {
    	List<Customer> customers =  entityManager.createNamedQuery(Customer.GET_ALL_QUERY_NAME).getResultList(); 
        customer.setCustomerId(customers.get(0).getCustomerId() + 1);
        entityManager.persist(customer);
    }

	@Override
	public List<Customer> searchCustomerByIndustry(String industry) throws Exception {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery query = builder.createQuery(Customer.class);
        Root<Customer> p = query.from(Customer.class);
        query.select(p).where(builder.equal(p.get("industry").as(String.class), industry));
        List<Customer> lp = entityManager.createQuery(query).getResultList();
		return lp;
	}
		

}
