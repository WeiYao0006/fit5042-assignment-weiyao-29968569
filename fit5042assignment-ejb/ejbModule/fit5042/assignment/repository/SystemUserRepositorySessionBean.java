package fit5042.assignment.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import fit5042.assignment.repository.entities.SystemUser;

@Stateless
public class SystemUserRepositorySessionBean implements SystemUserRepository{

	@PersistenceContext (unitName = "fit5042assignment-ejb")//unitName = persistence-unit in jebModule/META-INF/persistence.xml
	private EntityManager entityManager;

	@Override
	public List<SystemUser> getAllUser() throws Exception {
		return entityManager.createNamedQuery(SystemUser.GET_ALL_QUERY_NAME).getResultList();
	}

	@Override
	public SystemUser searchSystemUserById(int userId) throws Exception {
		SystemUser systemUser = entityManager.find(SystemUser.class, userId);
		return systemUser;
		
	}

	@Override
	public void removeUser(int userId) throws Exception {
		SystemUser systemUser = this.searchSystemUserById(userId);
		if (systemUser != null) {
			entityManager.remove(systemUser);
		}
	}

	@Override
	public void editUser(SystemUser systemUser) throws Exception {
		try {
			entityManager.merge(systemUser);
		} catch(Exception ex) {
			
		}
		
	}

	@Override
	public void addUser(SystemUser systemUser) throws Exception {
    	List<SystemUser> systemUsers =  entityManager.createNamedQuery(SystemUser.GET_ALL_QUERY_NAME).getResultList(); 
    	systemUser.setUserId(systemUsers.get(0).getUserId() + 1);
        entityManager.persist(systemUser);
    }

}
